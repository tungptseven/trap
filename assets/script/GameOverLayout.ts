// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class GameOverLayout extends cc.Component {
    gameScore: number = 0

    @property(cc.Label)
    scoreLbl: cc.Label = null

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.getChildByName('boardImg').getChildByName('replayBtn').active = false
        this.node.getChildByName('boardImg').getChildByName('homeBtn').active = false
    }

    onDisable() {
        this.node.getChildByName('boardImg').getChildByName('replayBtn').active = false
        this.node.getChildByName('boardImg').getChildByName('homeBtn').active = false
    }

    onEnable() {
        setTimeout(() => {
            this.node.getChildByName('boardImg').getChildByName('replayBtn').active = true
            this.node.getChildByName('boardImg').getChildByName('homeBtn').active = true
        }, 1000)
    }

    start() {

    }

    update(dt) {
        this.scoreLbl.string = this.gameScore.toString()
    }
}
