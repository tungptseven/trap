// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameScore: number = 0
    cooldown: number = 4
    gameOverLayout = null
    homeLayout = null
    gameLayout: GameLayout = null
    gunList = []
    selectedGun = []
    gunsShot = []

    @property(cc.Label)
    scoreLbl: cc.Label = null

    @property(cc.Node)
    gun1: cc.Node = null

    @property(cc.Node)
    gun2: cc.Node = null

    @property(cc.Node)
    gun3: cc.Node = null

    @property(cc.Node)
    gun4: cc.Node = null

    @property(cc.Node)
    gun5: cc.Node = null

    // LIFE-CYCLE CALLBACKS:
    gameOver() {
        this.selectedGun.forEach(t => t.children.find(node => node.name == 'bubbleImg').active = false)
        this.gunsShot.forEach(e => e.forEach(u => u.active = false))
        this.selectedGun = []
        this.gunsShot = []
        this.node.stopAllActions()
        this.gameStatus = GameStatus.Game_Over
        this.node.active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        this.node.getChildByName('Bat').setPosition(cc.v2(0, 0))
    }

    onPlay(isReplay: boolean = true) {
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = 0
        this.selectedGun = []
        this.gunsShot = []
    }

    onSpawn() {
        let numOfGuns = Math.floor(Math.random() * 4) + 1
        var shuffled = this.gunList.sort(() => { return .5 - Math.random() })
        this.selectedGun = shuffled.slice(0, numOfGuns)

        this.selectedGun.forEach(t => {
            this.gunsShot.push(t.children.filter(node => node.name == 'gunShotImg'))
            this.gunsShot.forEach(e => e.forEach(u => u.active = true))
        })

        // shot bubble
        setTimeout(() => {
            this.selectedGun.forEach(t => t.children.find(node => node.name == 'bubbleImg').active = true)
        }, 1500)

        // reload
        setTimeout(() => {
            this.gunsShot.forEach(e => e.forEach(u => u.active = false))
            this.selectedGun.forEach(t => t.children.find(node => node.name == 'bubbleImg').active = false)
        }, 2500)
        setTimeout(() => {
            this.selectedGun = []
            this.gunsShot = []
            this.gameScore += 1
        }, 3000)
    }

    onLoad() {
        var collisionManager = cc.director.getCollisionManager()
        collisionManager.enabled = true

        this.gameScore = 0
        this.scoreLbl.string = this.gameScore.toString()
        this.gameStatus = GameStatus.Game_Playing
        this.gunList = [this.gun1, this.gun2, this.gun3, this.gun4, this.gun5]
        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')
        this.homeLayout = cc.Canvas.instance.node.getChildByName('HomeLayout')
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
    }

    start() {

    }

    update(dt) {
        if (this.gameStatus === GameStatus.Game_Playing) {
            this.scoreLbl.string = this.gameScore.toString()
            this.cooldown -= dt
            if (this.cooldown <= 0) {
                this.onSpawn()
                this.cooldown = 4
            }
        }
    }
}
