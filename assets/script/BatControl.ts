// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameScene from "./GameScene"
import GameLayout from "./GameLayout"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class BatControl extends cc.Component {
    speed: number = 0
    gameControl: GameScene = null
    gameLayout: GameLayout = null

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        //game over
        if (other.tag === 0) {
            this.gameLayout.gameOver()
            this.speed = 0
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
        }
    }

    onLoad() {
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this)
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
    }

    start() {

    }

    update(dt: number) {
        this.speed -= 0.5
        this.node.y += this.speed

        if (this.node.y > 600 || this.node.y < -600) {
            this.gameLayout.gameOver()
            this.speed = 0
        }
    }

    onTouchStart(event: cc.Event.EventTouch) {
        this.speed = 16
        this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Fly)
    }

}
